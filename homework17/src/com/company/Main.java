package com.company;


import java.util.*;

public class Main {

    public static void main(String[] args) {
        int count = 0;
        Map<String, Integer> wordsMap = new HashMap<>();
        String string = "write your code here your code";

        System.out.println("Исходный текст- '"+string+"'");

        // создаем массив с словами из текста, разделитель пробел
        String[] wordsArray = string.split(" ");

        Set<String> wordsSet = new HashSet<>();
        // создаем список встречающихся слов вколлекции Set
        for (String word : wordsArray) {
            wordsSet.add(word);
        }
        Iterator<String> wordsSetIterator = wordsSet.iterator();
        // перебираем коллекцию Set и считаем количество слов в тексте
        while (wordsSetIterator.hasNext()) {
            String wd = wordsSetIterator.next();
            count = 0;
            // перебираем массив слов и считаем количество вхождение слова из Set
            for (int i = 0; i < wordsArray.length; i++) {
                if (wd.equals(wordsArray[i])) {
                    count++;
                }
            }
            // записываем слово и количество в коллекцию Map
            wordsMap.put(wd, count);

           // System.out.println(line + " " + count);
        }

        System.out.println("Введите слово для поиска, выход - просто ENTER");
        Scanner in = new Scanner(System.in);
        String inWord = in.nextLine();


        while (inWord != "") {
            if (wordsMap.containsKey(inWord)) {
                System.out.println("Слово '"+ inWord +"' встречается в тексте "  + wordsMap.get(inWord).toString()+ " раз(а)");
            } else {
                System.out.println("слово '" + inWord + "' не найдено");
            }
            System.out.println("Введите следующее слово, выход - просто ENTER");
            inWord = in.nextLine(); // читаем сдедующее слово
        }

    }
}
