package com.company;

import java.util.List;

public interface UsersRepository {
    List<User> findAll();
    List<User> findAge(int age);
    List<User> findByIsWorkerIsTrue();
    void save(User user);
    User findById(int id);
    void update(User user);
    // void newUserToFile(User newUser);



}
