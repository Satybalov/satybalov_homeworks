package com.company;

import java.io.*;
import java.util.*;

import java.util.stream.Collectors;


public class UserRepositeryFileImpl<userList> implements UsersRepository {

    private String fileName;

    public UserRepositeryFileImpl(String fileName) {
        this.fileName = fileName;
    }

    @Override // получение всех пользователей из файла
    public List<User> findAll() {
        List<User> users = new ArrayList<>();
        // объявили переменные для доступа
        try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {

            users = reader.lines()
                    .map(line -> line.split("\\|"))
                    .map(line -> (new User(Integer.parseInt(line[0]), line[1], Integer.parseInt(line[2]), Boolean.parseBoolean(line[3]))))
                    .collect(Collectors.toList());

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();

        }

        return users;
    }


    @Override // получение списка пользователей определенного возраста
    public List<User> findAge(int age) {
        List<User> userList = new ArrayList<>(); // массив для хранения пользователей
        userList = findAll();// записываем в список всех людей из файла
        // создаем новый спиок для записа результата
        List<User> userListByAge = new ArrayList<>();
        // перебираем список и отбираем определенного возраста
        for (User user : userList) {
            if (user.getAge() == age) {
                userListByAge.add(user);
            }

        }
        return userListByAge;

    }

    @Override // получение списка пользователей которые работают
    public List<User> findByIsWorkerIsTrue() {
        List<User> userList = new ArrayList<>(); // массив для хранения людей
        userList = findAll();// записываем в список всех людей из файла
        // в стриме отбираем тех кто работает
        return userList.stream().filter(usr -> usr.isWorker()).collect(Collectors.toList())   ;
    }

    @Override // сохранение нового пользователя
    public void save(User user) {

        newUserToFile(user);

    }

    /** Додавляем нового пользователя в файл
     * @param newUser новый пользователь
     */

    public void newUserToFile(User newUser) {
        // считываем всех пользователей
        List<User> userList = new ArrayList<>(); // массив для хранения пользователей
        userList = findAll();// считываем  в список всех пользоателей
        //emps.stream().max(Comparator.comparingInt(Employee::getAge));

        // определяем максимальный Id
        Optional<Integer> maxIdUserOpt = userList.stream().map(user -> user.getId()).max(Integer::compare);
        int maxIdUser = maxIdUserOpt.get();
        //  новому пльзователю присваиваем Id = Max+1
        newUser.setId(maxIdUser+1);
        userList.add(newUser);
        // перезаписываем файл c изменеными данными
        writeUserListToFile(userList);
    }

    /**
     * поиск пользователя по id
     * findById(1) вернет объект user с данными указанной строки.
     *
     * @param id код пользователя
     * @return User
     */
    @Override
    public User findById(int id) {

        List<User> userList = new ArrayList<>(); // массив для хранения людей
        userList = findAll();// записываем в список всех людей из файла

        return userList.stream().filter(userFind -> userFind.getId() == id).findFirst().orElse(null);
    }

    /**
     * Обновление данных пользовтеля с записью изменений в файл
     *
     * @param user
     */
    @Override
    public void update(User user) {

        // считываем всех пользователей
        List<User> userList = new ArrayList<>(); // массив для хранения пользователей
        userList = findAll();// считываем  в список всех пользоателей

        // находим в коллекции пользователя по id2
        for (User usr : userList) {
            // если пользователь найден то меняем ему данные
            if (usr.getId() == user.getId()) {
                usr.setName(user.getName());
                usr.setAge(user.getAge());
                usr.setWorker(user.isWorker());
                break;
            }
        }
        // перезаписываем файл c изменеными данными в файл
        writeUserListToFile(userList);
    }

    /** данные с коллекции List<> записываем в текстовый файл
     *
     * @param userList
     */
    private void writeUserListToFile(List<User> userList) {
        // перезаписываем файл c изменеными данными
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(fileName))) {
            String str = new String();
            for (User usr : userList) {
                str = usr.getId() + "|" + usr.getName() + "|" + usr.getAge() + "|" + usr.isWorker();
                writer.write(str);
                writer.newLine();
            }
            System.out.println("Данные перезаписаны!");

        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }




}
