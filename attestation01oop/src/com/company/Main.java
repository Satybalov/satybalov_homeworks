package com.company;

import java.util.List;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        UsersRepository usersRepository = new UserRepositeryFileImpl("users.txt");
        // получаем коллекцию всех пользователей из файла
        List<User> userList;//= usersRepository.findAll();
        User user = new User();

        System.out.println("Введите действие: ");
        System.out.println("1 - вывести весь список");
        System.out.println("2 - вывести пользователей определенного возраста");
        System.out.println("3 - вывести список имеющих постоянную работу");
        System.out.println("4 - найти полльзователя с номером Id");
        System.out.println("5 - добавить нового пользователя");
        System.out.println("0 - выход");

        Scanner scanner = new Scanner(System.in);
        // проверяем ввод на число
        while (!scanner.hasNextInt()) {
            System.out.println("Введено не корректное значение!");
            System.out.println("Повторите ввод!");
            scanner.next();
        }
        int selNumber = scanner.nextInt();

        while (selNumber != 0) {
            switch (selNumber) {
                case 1: { // вывести весь список
                    userList = usersRepository.findAll();
                    printUserListToTerminal(userList);
                    break;

                }
                case 2: {//вывести пользователей определенного возраста
                    System.out.print("Ведите возраст = ");

                    // проверяем ввод на число
                    while (!scanner.hasNextInt()) {
                        System.out.println("Введено не корректное значение!");
                        System.out.println("Повторите ввод!");
                        scanner.next();
                    }
                    int age = scanner.nextInt();

                    List<User> userListAge = usersRepository.findAge(age);
                    printUserListToTerminal(userListAge);
                    break;

                }
                case 3: {//вывести список имеющих постоянную работу
                    List<User> userListIsWorker = usersRepository.findByIsWorkerIsTrue();
                    printUserListToTerminal(userListIsWorker);
                    break;
                }
                case 4: { //найти полльзователя с номером Id
                    System.out.print("Ведите номер Id = ");
                    // проверяем ввод на число
                    while (!scanner.hasNextInt()) {
                        System.out.println("Введено не корректное значение!");
                        System.out.println("Повторите ввод!");
                        scanner.next();
                    }
                    int id = scanner.nextInt();
                    User userFind = new User();
                    userFind = usersRepository.findById(id);
                    if (userFind != null) {
                        user = userFind;
                        System.out.println("Пользователь найден:");
                        System.out.println(user.getId() + " " + user.getName() + " "
                                + user.getAge() + " " + user.isWorker());
                    } else {
                        System.out.println("Пользователь c id =" + id + " не найден!");
                    }
                    // выбираем действие, что будем делать с найденным пользователем
                    System.out.println("Изменить данные пользователя? 1-Да, 0-Нет");
                    // проверяем ввод на число
                    while (!scanner.hasNextInt()) {
                        System.out.println("Введено не корректное значение!");
                        System.out.println("Повторите ввод!");
                        scanner.next();
                    }
                    int selUpdate = scanner.nextInt();

                    if (selUpdate == 1) {
                        System.out.println("Введите новое имя:");
                        String newName = scanner.next();
                        user.setName(newName);
                        usersRepository.update(user);
                    }
                    break;
                }
                case 5: { // добавление нового пользователя
                    User newUser = new User();
                    System.out.println("Введите имя:");
                    newUser.setName(scanner.next());

                    System.out.println("Введите возраст:");
                    // проверяем ввод на число
                    while (!scanner.hasNextInt()) {
                        System.out.println("Введено не корректное значение!");
                        System.out.println("Повторите ввод!");
                        scanner.next();
                    }
                    newUser.setAge(scanner.nextInt());

                    System.out.println("работает-1, не работает -0:");
                    // проверяем ввод на число
                    while (!scanner.hasNextInt()) {
                        System.out.println("Введено не корректное значение!");
                        System.out.println("Повторите ввод!");
                        scanner.next();
                    }
                    if (scanner.nextInt() == 1) {
                        newUser.setWorker(true);
                    }
                    ;

                    usersRepository.save(newUser);
                    break;
                }

            }
            System.out.println(" введите действие(0 -выход)");
            selNumber = scanner.nextInt();
        }
        System.out.println();
    }

    /**
     * Выводим на экран терминал список пользоватей
     *
     * @param userList список пользователей
     */
    private static void printUserListToTerminal(List<User> userList) {
        if (userList.isEmpty()) {  // если список пуст то
            System.out.println("Список пуст!");
            return;
        }
        System.out.println("|Код|Имя|Возр|Работа|");
        for (User usr : userList) {
            System.out.println(usr.getId() + " " + usr.getName() + " " + usr.getAge() + " " + usr.isWorker());
        }
        System.out.println();
    }
}
