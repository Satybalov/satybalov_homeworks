package com.company;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.OptionalDouble;

/**
 * 17.11.2021
 * 21. Stream API
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) {

        try {

            BufferedReader reader = new BufferedReader(new FileReader("cars.txt"));
            //выводим строки из файла
            System.out.println("номера автомобилей с черным цветом или нулевым пробегом: ");
            reader.lines()
                    .map(line -> line.split("\\|"))
                    .filter(line -> (line[2].equals("Black") || Integer.parseInt(line[3]) == 0))
                    .forEach(line -> System.out.println(line[0] + " " + line[1] + " " + line[2] + " " + line[3] + " " + line[4]));


            BufferedReader reader2 = new BufferedReader(new FileReader("cars.txt"));
            System.out.print("Количество уникальных моделей в ценовом диапазоне от 700 до 800 тыс. = ");

            long count = reader2.lines()
                    .map(line -> line.split("\\|"))
                    .filter(line -> (Integer.parseInt(line[4]) >= 700000 && Integer.parseInt(line[4]) <= 800000)) // фильтруем по цене
                    .map(line -> line[1])// отбираем только название автомобилей
                    .distinct() // оставляем только уникальные названия
                    .count(); // считаем количество строк
            // .forEach(line -> System.out.println(line)); // печатаем список автомобилей
            System.out.println(count + " автомобиля(ей)");

            BufferedReader reader3 = new BufferedReader(new FileReader("cars.txt"));
       
            System.out.print("средняя стоимость автомобиля Camry =  ");

            OptionalDouble aver = reader3.lines()
                    .map(line -> line.split("\\|"))
                    .filter(line -> (line[1].equals("Camry"))) // фильтруем по названию
                    .mapToInt(line -> Integer.parseInt(line[4]))// преобразуем строку в числовой стрим
                    .average(); // вычисляем среднее

            System.out.println(aver.getAsDouble() + " рубля(ей)");


        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }


    }
}
