package com.company;

import java.util.Random;
import java.util.Scanner;

public class Main {
    public static int array[];
    public static int sumFromThread[];

    public static void main(String[] args) {
        Random random = new Random();

        System.out.print("Введите длину массива = ");
        Scanner scanner = new Scanner(System.in);
        int numberCount = scanner.nextInt();
        System.out.print("Введите кодичество потоков для обработки = ");
        int threadsCount = scanner.nextInt();

        sumFromThread = new int[threadsCount];//создаем массив для хранения результатов вычисления потоков

        // создаем числовой массив, заполняем случайными данными и считаем сумму в текущем потоке
        array = new int[numberCount];
        int realSum = 0;
        for (int i = 0; i < array.length; i++) {
            array[i] = random.nextInt(100);
            realSum += array[i];
        }
        System.out.println("Реальная сумма = " + realSum);

        // считаем сколько элементов надо передать в каждый поток для расчета суммы
        int numbersElementInThread = numberCount / threadsCount; // количество элементов для расчета
        int startIndex = 0; // начальный передаваемый в поток индекс массива для расчета
        int endIndex = 0;    // конечный передоваемый в поток индекс массива

        // создаем массив потоков
        Thread[] threadArray = new Thread[threadsCount];

        // создаем и запускаем потоки
        for (int i = 0; i < threadsCount; i++) {

            // расчитываем начальные и конечные индексы массива для передачи в расчет
            startIndex = numbersElementInThread * i;
            if (i == (threadsCount - 1)) { // если это последний кусок то берем до последнего индека массива
                endIndex = array.length - 1;
            } else {
                endIndex = numbersElementInThread * (i+1)-1;
            } ;

            // создаем поток и записываем в массив
            threadArray[i] = new Thread(new CountSumThread( startIndex, endIndex,i));
            // запускаем поток
            threadArray[i].start();
        }

        for (int i = 0; i < threadsCount; i++) {
            try {
                threadArray[i].join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        
        int byThreadSum = 0; // итоговая сумма расчитанная в потоках
        // суммируем результаты каждого потока
        for (int i = 0; i < threadsCount; i++) {
            byThreadSum += sumFromThread[i];
        }


        System.out.println("расчитанная сумма = " + byThreadSum);


    }
}
