package com.company;

import static com.company.Main.array;
import static com.company.Main.sumFromThread;

public class CountSumThread extends  Thread{
//    private int newArray[];
    private int from;
    private int to;
    private int id ;
    //   private int sumThreadArray=0;


  //  public CountSumThread(int[] array, int from, int to,int id ) {
      public CountSumThread( int from, int to,int id ) {
        super("thread_from_"+from+"_to_"+to);

        this.from = from;
        this.to = to;
        this.id = id; // id потока
    }

    @Override
    public void run() {
        for (int i = from; i <= to; i++) {
            sumFromThread[id] += array[i];
        };

    }

}
