import java.util.Scanner;

class Program {
	public static void main(String[] args) {


		// объявляю объект для считывания чисел с консоли
		Scanner scanner = new Scanner(System.in);

		// считываю первое число
		int a = scanner.nextInt();
			// объявляю переменные и задаю им начальные значения
		int minCifra = -1; // минимальная цифра делаем равной -1 для начала отсчета
		int sledCifra = 0;

		// пока не встретили -1
		while (a != -1) {
			// перебираем цифры
			while (a != 0) {
				// запоминаем последную цифру
				sledCifra = a % 10;
				// сравниваем с минимальным

				if ((sledCifra < minCifra) | (minCifra < 0)) {
					minCifra = sledCifra;
				}
				// отбрасываем от исходного числа последнюю цифру
				a = a / 10;
			}

			// на каждом шаге цикла считываем новое число из консоли
			a = scanner.nextInt();
		}
		// выводим результат
		System.out.println("Min cifra = " + minCifra);
	}
}
