package com.example.homework28.controller;

import com.example.homework28.models.Product;
import com.example.homework28.repositories.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class ProductsController {
    @Autowired
    ProductRepository productRepository;

    @PostMapping("/products")
    public String AddProduct(@RequestParam("description") String description
            ,@RequestParam("price") Double price
            ,@RequestParam("amount") Integer amount){

        Product product = Product.builder()
                .description(description)
                .price(price)
                .amount(amount)
                .build();
        productRepository.save(product);

      // System.out.println(description);
        return "redirect:/product_add.html";
    }
}
