package com.company;

public class Human {
    private   String nameHuman; // Имя
    private   double weightHuman; // Вес

    public Human(String nameHuman, double weightHuman){
        this.nameHuman = nameHuman;
        if (weightHuman < 0) {
            weightHuman = 0;
        }
        this.weightHuman = weightHuman;
    }

    public String getNameHuman() {
        return this.nameHuman;
    }

    public double getWeightHuman() {
        return this.weightHuman;
    }

    public void setNameHuman(String nameHuman) {
        this.nameHuman = nameHuman;
    }

    public void setWeightHuman(double weightHuman) {
        if (weightHuman < 0) {
            weightHuman = 0;
        }
        this.weightHuman = weightHuman;
    }



}
