package com.company;

public class Main {

    public static void main(String[] args) {

        //объявляем массив
        Human[] human = new Human[10] ;

        // инициируем массив и заполним данными, вес случайным оброзом
        for (int index=0; index<10;index++ ){
            human[index] = new Human ("User"+index, Math.random()*100);
            //   System.out.println(human[index].getNameHuman()+"   "+ human[index].getWeightHuman());
        }

        human = sortAndPrintHumans(human);  // сортируем и выводим массив

    }

    public static Human[] sortAndPrintHumans(Human[] human) {
        // Сортируем массив по весу
        Human tempWeightHuman = new Human("",0);

        for (int index = 0; index< human.length; index++ ){

            int minIndex = index;
            double minWeight = human[index].getWeightHuman();

            for (int indexSecond = index+1; indexSecond < human.length; indexSecond++) {
                // если возраст меньше минимального
                if (human[indexSecond].getWeightHuman() < minWeight){
                    minIndex = indexSecond;
                    minWeight = human[indexSecond].getWeightHuman();
                }
            }
            // меняем местами элементы массива
           tempWeightHuman.setNameHuman(human[index].getNameHuman()) ;
           tempWeightHuman.setWeightHuman(human[index].getWeightHuman());
           human[index].setNameHuman(human[minIndex].getNameHuman()) ;
           human[index].setWeightHuman(human[minIndex].getWeightHuman());
           human[minIndex].setNameHuman(tempWeightHuman.getNameHuman()) ;
           human[minIndex].setWeightHuman(tempWeightHuman.getWeightHuman());

           System.out.printf("Имя- %s вес- %.2f кг. \n", human[index].getNameHuman(), human[index].getWeightHuman());
        }
        return human; // возвращаем отсортированный массив
    }
}
