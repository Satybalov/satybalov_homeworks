package com.company;

/**
 *  Паттерн Singleton Вывод лога на экран
 */
public class Logger {

    private static final Logger instance;

    static {
        instance = new Logger();
    }

    private Logger() {
    }

    public static Logger getInstance(){
        return instance;
    };

    public void log(String message){ //реализация логирования
        // выводим сообщение на экран
        System.out.println(message);
    }



}
