package com.example.rental.models;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Apartment {

    private Integer id;
    private Integer owner_id;
    private String  city;
    private String  street;
    private String house;
    private Integer room_count;
    private Double price;
    private Double rating;
    private String description;




}
