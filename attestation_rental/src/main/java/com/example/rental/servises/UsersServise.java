package com.example.rental.servises;

import com.example.rental.forms.UserForm;
import com.example.rental.models.User;

import java.util.List;

public interface UsersServise {
    void addUser(UserForm form);
    List<User> getAllUsers();

    void deleteUser(Integer userid);

    User getUser(Integer userId);
}
