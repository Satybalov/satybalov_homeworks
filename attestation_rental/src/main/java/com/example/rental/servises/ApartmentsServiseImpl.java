package com.example.rental.servises;

import com.example.rental.forms.ApartmentForm;
import com.example.rental.models.Apartment;
import com.example.rental.repositories.ApartmentsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ApartmentsServiseImpl implements  ApartmentsServise{

    private  final ApartmentsRepository apartmentsRepository;

    @Autowired
    public ApartmentsServiseImpl( ApartmentsRepository apartmentsRepository) {
        this.apartmentsRepository = apartmentsRepository;
    }

    @Override
    public void addApartment(ApartmentForm form) {
        Apartment apartment = Apartment.builder()
                .owner_id(form.getOwner_id())
                .city(form.getCity())
                .street(form.getStreet())
                .house(form.getHouse())
                .room_count(form.getRoom_count())
                .rating(form.getRating())
                .price(form.getPrice())
                .description(form.getDescription())
                .build();
        apartmentsRepository.save(apartment);


    }

    @Override
    public List<Apartment> getAllApartments() {

        return  apartmentsRepository.findAll();
    }

    @Override
    public void deleteApartment(Integer apartmentId) {
        apartmentsRepository.delete(apartmentId);
    }

    @Override
    public Apartment getApartment(Integer apartmentId) {
        return apartmentsRepository.findById(apartmentId);
    }
}
