package com.example.rental.servises;

import com.example.rental.forms.UserForm;
import com.example.rental.models.User;
import com.example.rental.repositories.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class UsersServiseImpl implements UsersServise {

    private UsersRepository usersRepository;

    @Autowired
    public UsersServiseImpl(UsersRepository usersRepository) {
        this.usersRepository = usersRepository;
    }

    @Override
    public void addUser(UserForm form) {
         User user = User.builder()
                .firstName(form.getFirstName())
                .lastName(form.getLastName())
                .email(form.getEmail())
                .password(form.getPassword())
                .rol(form.getRol())
                .build();
        usersRepository.save(user);
    }

    @Override
    public List<User> getAllUsers() {
        return usersRepository.findAll();
    }

    @Override
    public void deleteUser(Integer userid) {
        usersRepository.delete(userid);
    }

    @Override
    public User getUser(Integer userid) {
        return usersRepository.findById(userid);
    }
}
