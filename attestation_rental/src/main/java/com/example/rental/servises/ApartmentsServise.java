package com.example.rental.servises;

import com.example.rental.forms.ApartmentForm;
import com.example.rental.models.Apartment;
import org.springframework.stereotype.Component;

import java.util.List;


public interface ApartmentsServise {
    void addApartment(ApartmentForm form);

    List<Apartment> getAllApartments();

    void deleteApartment(Integer apartmentId);

    Apartment getApartment(Integer apartmentId);
}
