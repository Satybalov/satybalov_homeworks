package com.example.rental.controller;

import com.example.rental.forms.ApartmentForm;
import com.example.rental.models.Apartment;
import com.example.rental.repositories.ApartmentsRepository;

import com.example.rental.servises.ApartmentsServise;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Controller
public class ApartmentController {

    private final ApartmentsServise apartmentsServise;

    @Autowired
    public ApartmentController(ApartmentsServise apartmentsServise) {
        this.apartmentsServise = apartmentsServise;
    }

    @GetMapping ("/apartments") // вывод апартаментов
    public String getApartmentPage(Model model ){
        List<Apartment> apartments = apartmentsServise.getAllApartments();
        model.addAttribute("apartments",apartments);
        return  "apartments";
    }

    @PostMapping("/apartments") // добавление новых апартаментов
    public String addApartment(ApartmentForm form ){
        apartmentsServise.addApartment(form);
       return  "redirect:/apartments";
    }

    @PostMapping("/apartments/{apartment-id}/delete") // удаление апартамента
    public String deleteApartment(@PathVariable("apartment-id") Integer apartmentId){
        apartmentsServise.deleteApartment(apartmentId);
        return "redirect:/apartments";
    }

    @GetMapping("/apartments/{apartment-id}") // вызов страницы отображения и редактирования апартаментов
    public String getApartmentPage(Model model, @PathVariable( "apartment-id") Integer apartmentId){
        Apartment apartment = apartmentsServise.getApartment(apartmentId);
        model.addAttribute("apartment", apartment);
    return "apartment";
    }


}
