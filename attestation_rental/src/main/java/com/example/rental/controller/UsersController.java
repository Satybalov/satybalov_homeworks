package com.example.rental.controller;

import com.example.rental.forms.UserForm;
import com.example.rental.models.User;
import com.example.rental.repositories.UsersRepository;
import com.example.rental.servises.UsersServise;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.List;

@Controller
public class UsersController {

    private final UsersServise usersServise;

    @Autowired
    public UsersController(UsersServise usersServise) {
        this.usersServise = usersServise;
    }

    @GetMapping("/users") // вывести пользователей
    public String getUserPage(Model model){
        List<User> users = usersServise.getAllUsers();
        model.addAttribute("users",users);
        return "users";
    }

    @PostMapping("/users") // добавить нового пользователя
    public String addUser(UserForm form){
        usersServise.addUser(form);
        return "redirect:/users";
    }

    @PostMapping("/users/{user-id}/update") // изменить данные пользователя
    public String updateUser(@PathVariable("user-id") Integer userid){
     //   usersServise.updateUser(userid);
        return "redirect:/user";
    }


    @PostMapping("/users/{user-id}/delete") // удалить пользователя
    public String deleteUser(@PathVariable("user-id") Integer userid){
        usersServise.deleteUser(userid);
        return "redirect:/users";
    }
    @GetMapping("/users/{user-id}") // вызов страницы отображения и редактирования пользоввателя
    public String getUserPage(Model model, @PathVariable("user-id") Integer userId) {
        User user = usersServise.getUser(userId);
        model.addAttribute("user", user);
        return "user";
    }

}
