package com.example.rental.repositories;

import com.example.rental.models.User;

import java.util.List;

public interface UsersRepository {
    List<User> findAll();

    void save(User user);

    void delete(Integer userid);

    User findById(Integer userid);
}
