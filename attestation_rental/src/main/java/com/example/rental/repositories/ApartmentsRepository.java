package com.example.rental.repositories;

import com.example.rental.models.Apartment;


import java.util.List;

public interface ApartmentsRepository {

    List<Apartment> findAll();

    void save(Apartment apartment);

    void delete(Integer apartmentId);

    Apartment findById(Integer apartmentId);
}
