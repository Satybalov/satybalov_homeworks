package com.example.rental.repositories;

import com.example.rental.models.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.util.List;

@Component
public class UsersRepositoryJdbcTemplateImpl implements UsersRepository {

    //language=SQL
    private static final String SQL_INSERT = "INSERT INTO account(first_name, last_name, email, password, rol) " +
            " VALUES(?, ?, ?, ?, ?)";

    //language=SQL
    private static final String SQL_SELECT_ALL = "SELECT * FROM account ORDER BY id";
    //language=SQL
    private static final String SQL_DELETE_BY_ID = "DELETE FROM account WHERE id = ?";
    //language=SQL
    private static final String SQL_SELECT_BY_ID = "SELECT * FROM account WHERE id = ?";


    private JdbcTemplate jdbcTemplate;

    @Autowired
    public UsersRepositoryJdbcTemplateImpl(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }
        // правило формирования экземпляра класса из строки
    private static final RowMapper<User> userRowMapper = (row, rowNumber) -> {
        int id = row.getInt("id");
        String first_name = row.getString("first_name");
        String last_name = row.getString("last_name");
        String email = row.getString("email");
        String password = row.getString("password");
        String rol = row.getString("rol");
        return new User(id, first_name, last_name, email, password, rol);
    };

    @Override
    public List<User> findAll() {
        return jdbcTemplate.query(SQL_SELECT_ALL, userRowMapper);
    }

    @Override
    public void save(User user) {
        jdbcTemplate.update(SQL_INSERT, user.getFirstName(), user.getLastName(), user.getEmail()
                , user.getPassword(), user.getRol());
    }

    @Override
    public void delete(Integer userid) {
        jdbcTemplate.update(SQL_DELETE_BY_ID, userid);
    }

    @Override
    public User findById(Integer userid) {
        return jdbcTemplate.queryForObject(SQL_SELECT_BY_ID, userRowMapper, userid);
    }
}
