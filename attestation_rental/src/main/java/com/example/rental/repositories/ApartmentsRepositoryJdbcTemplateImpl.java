package com.example.rental.repositories;

import com.example.rental.models.Apartment;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.util.List;

@Component
public class ApartmentsRepositoryJdbcTemplateImpl implements ApartmentsRepository {
    //language=SQL
    private static final String SQL_INSERT = "INSERT INTO apartment (owner_id, city, street, house, room_count" +
            ", price, rating, description) " +
            " VALUES(?, ?, ?, ?, ?, ?, ?, ?)";

    //language=SQL
    private static final String SQL_SELECT_ALL = "SELECT * FROM apartment ORDER BY id";
    //language=SQL
    private static final String SQL_DELETE_BY_ID = "DELETE FROM apartment WHERE id = ?";
    //language=SQL
    private static final String SQL_SELECT_BY_ID = "SELECT * FROM apartment WHERE id = ?";

    private JdbcTemplate jdbcTemplate;


    @Autowired
    public ApartmentsRepositoryJdbcTemplateImpl(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    private static RowMapper<Apartment> apartmentRowMapper = (row, rowNumber) -> {

        int id = row.getInt("id");
        int owner_id = row.getInt("owner_id");
        String city = row.getString("city");
        String street = row.getString("street");
        String house = row.getString("house");
        int room_count = row.getInt("room_count");
        Double price = row.getDouble("price");
        Double rating = row.getDouble("rating");
        String description = row.getString("description");

        return new Apartment(id, owner_id, city, street, house, room_count, price, rating, description);


    };


    @Override
    public List<Apartment> findAll() {
        return jdbcTemplate.query(SQL_SELECT_ALL, apartmentRowMapper);
    }

    /**
     * Добавление новой квартиры
     *
     * @param apartment
     */
    @Override
    public void save(Apartment apartment) {
        jdbcTemplate.update(SQL_INSERT, apartment.getOwner_id(), apartment.getCity(), apartment.getStreet()
                , apartment.getHouse(), apartment.getRoom_count(), apartment.getPrice()
                , apartment.getRating(), apartment.getDescription());
    }

    @Override
    public void delete(Integer apartmentId) {
         jdbcTemplate.update(SQL_DELETE_BY_ID,apartmentId);
    }

    @Override
    public Apartment findById(Integer apartmentId) {
        return jdbcTemplate.queryForObject(SQL_SELECT_BY_ID,apartmentRowMapper,apartmentId);
    }
}
