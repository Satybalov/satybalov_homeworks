package com.example.rental.forms;

import lombok.Data;

@Data
public class ApartmentForm {
    private Integer id;
    private Integer owner_id;
    private String  city;
    private String  street;
    private String house;
    private Integer room_count;
    private Double price;
    private Double rating;
    private String description;
}
