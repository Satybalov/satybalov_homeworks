package com.example.rental.forms;

import lombok.Data;

@Data
public class UserForm {
    private String firstName;
    private String lastName;
    private String email;
    private String password;
    private String rol;

}
