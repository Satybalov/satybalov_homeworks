-- создание таблицы
CREATE TABLE account -- пользователи
(
    id         SERIAL PRIMARY KEY,
    -- определяем столбцы
    first_name VARCHAR(30),
    last_name  VARCHAR(30),
    email      VARCHAR(30),
    password   VARCHAR(50),
    rol        VARCHAR(10)

);

-- добавление информации
INSERT INTO account(first_name, last_name, email, password, rol)
VALUES ('Марсель', 'Сидиков', 'sididov@mail.ru', '12345678', 'CLIENT');

INSERT INTO account(first_name, last_name, email, password, rol)
VALUES ('Геннадий', 'Сатыбалов', 'satybalov@mail.ru', '12345678', 'ADMIN');

INSERT INTO account(first_name, last_name, email, password, rol)
VALUES ('Василий', 'Пупкин', 'pupkin@mail.ru', '12345678', 'RENTER');



CREATE TABLE apartment
(
    id          SERIAL PRIMARY KEY,
    owner_id    INTEGER,      -- id владельца
    city        VARCHAR(30),  -- город
    street      VARCHAR(30),  -- улица
    house       VARCHAR(4),  -- номер дома
    room_count        INTEGER,      -- количество комнат
    price       NUMERIC,-- стоиость проживания за сутки
    rating      NUMERIC,      -- средний райтинг жилья вычисляется после каждого отзыва
    description VARCHAR(100), -- описание квартиры
    -- внешний ключ, ссылка на строку из другой таблицы
    FOREIGN KEY (owner_id) REFERENCES account (id)
);


INSERT INTO apartment (owner_id, city, street, house, room_count, rating, price, description)
VALUES (3, 'Альметьевск', 'Галеева', 27, 2, 0, 1500.0, 'Комфортная уютная квартира ');

INSERT INTO apartment(owner_id, city, street, house, room_count, rating, price, description)
VALUES (3, 'Альметьевск', 'Ленина', 123, 1, 0, 1700.0, 'Комфортная уютная квартира с домашним уютом ');

CREATE TABLE rent
(
    id           SERIAL PRIMARY KEY,
    apartment_id INTEGER,      -- id квартиры
    client_id    INTEGER,      -- код клиента из account
    check_in     DATE,         -- дата заселения
    check_out    DATE,         -- дата выезда
    cost         NUMERIC,-- стоиость проживания за выбранное время
    rating       NUMERIC,      --  рейтинг выставленный клиентом
    feedback     VARCHAR(100), -- отзыв оставленный клиентом

    -- внешний ключ, ссылка на строку из другой таблицы
    FOREIGN KEY (apartment_id) REFERENCES apartment (id),
    FOREIGN KEY (client_id) REFERENCES account (id)
);

INSERT INTO rent (apartment_id, client_id, check_in, check_out, cost, rating, feedback)
VALUES (2, 1, '2021-12-12', '2021-12-13', 1700.0, 5, 'Все было отлично');

INSERT INTO rent (apartment_id, client_id, check_in, check_out, cost, rating, feedback)
VALUES (1, 2, '2021-12-12', '2021-12-13', 1500.0, 4, 'Все было хорошо');


