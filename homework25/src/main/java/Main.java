import org.springframework.jdbc.datasource.DriverManagerDataSource;

import javax.sql.DataSource;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        DataSource dataSource = new DriverManagerDataSource("jdbc:postgresql://localhost:5432/myPostgres", "postgres", "12345678");
        //   JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        ProductRepository productRepository = new ProductRepositoryJdbcTemplateImpl(dataSource);

        System.out.println("Введите действие: ");
        System.out.println("1 - вывести весь список продуктов");
        System.out.println("2 - вывести список продуктов определенной цены");
        System.out.println("3 - Добавление нового продукта");
        System.out.println("0 - выход");
        Scanner scanner = new Scanner(System.in);
        // проверяем ввод на число
        while (!scanner.hasNextInt()) {
            System.out.println("Введено не корректное значение!");
            System.out.println("Повторите ввод!");
            scanner.next();
        }
        int selNumber = scanner.nextInt();

        while (selNumber != 0) {
            switch (selNumber) {
                case 1: { // вывести весь список

                    System.out.println(productRepository.findAll());
                    break;

                }
                case 2: {//вывести продукты определенной стоимости
                    System.out.print("Ведите стоимось = ");

                    // проверяем ввод на число
                    while (!scanner.hasNextDouble()) {
                        System.out.println("Введено не корректное значение!");
                        System.out.println("Повторите ввод!");
                        scanner.next();
                    }
                    double price = scanner.nextDouble();

                    System.out.println(productRepository.findAllByPrice(price));

                    break;

                }
                case 3: {//вывести продукты определенной стоимости

                    System.out.println("Введите название продукта: ");
                    String descr = scanner.next();

                    System.out.print("Ведите стоимось ед. - " + descr + ":");
                    // проверяем ввод на число
                    while (!scanner.hasNextDouble()) {
                        System.out.println("Введено не корректное значение!");
                        System.out.println("Повторите ввод!");
                        scanner.next();
                    }
                    double price = scanner.nextDouble();

                    System.out.println("Введите количество:");
                    // проверяем ввод на число
                    while (!scanner.hasNextInt()) {
                        System.out.println("Введено не корректное значение!");
                        System.out.println("Повторите ввод!");
                        scanner.next();
                    }
                    int amount = scanner.nextInt();

                    Product product = Product.builder()
                            .description(descr)
                            .price(price)
                            .amount(amount)
                            .build();

                    //  Product product = new Product("Помидор", 85, 130);
                    productRepository.save(product);
                    System.out.println("Запись добавлена!");
                    break;
                }
            }
            System.out.println(" Введите действие(0 -выход)");
            selNumber = scanner.nextInt();


        };
    }
}


