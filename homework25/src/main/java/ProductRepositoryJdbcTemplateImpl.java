import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import javax.sql.DataSource;
import java.util.List;

public class ProductRepositoryJdbcTemplateImpl implements ProductRepository {
    // language = SQL
    private static final String SQL_INSERT_PRODUCT = "insert into product (description, price, amount) VALUES (?, ?, ?)";
    // language = SQL
    public static final String SQL_SELECT_ALL_PRODUCT = "select * from product order by id ";
    // language = SQL
    public static final String SQL_SELECT_PRODUCT_BY_PRICE = "select * from product where price = ";


    private JdbcTemplate jdbcTemplate;

    public ProductRepositoryJdbcTemplateImpl(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    private static final RowMapper<Product> productRowMapper = (rs, rowNum) -> {
        int id = rs.getInt("id");
        String description = rs.getString("description");
        double price = rs.getDouble("price");
        int amount = rs.getInt("amount");
        return new Product(id, description, price, amount);
    };

    @Override
    public List<Product> findAll() {

        return jdbcTemplate.query(SQL_SELECT_ALL_PRODUCT, productRowMapper);
    }

    @Override
    public List<Product> findAllByPrice(Double price) {

        return jdbcTemplate.query(SQL_SELECT_PRODUCT_BY_PRICE + price , productRowMapper);
    }

    @Override
    public void save(Product product) {
        jdbcTemplate.update(SQL_INSERT_PRODUCT, product.getDescription(), product.getPrice(), product.getAmount());


    }
}
