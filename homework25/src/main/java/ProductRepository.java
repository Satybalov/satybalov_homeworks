import java.util.List;

public interface ProductRepository {
    List<Product> findAll();
    List<Product> findAllByPrice(Double price);

    void  save(Product product);
}
