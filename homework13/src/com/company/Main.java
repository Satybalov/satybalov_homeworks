package com.company;

import java.util.Arrays;

public class Main {

    public static void main(String[] args) {
        // создаем массив с числами
        int[] array = { 125, 46, 248, 475, 59, 16, 3, 4 };
        int[] res = new int[array.length];

        ByCondition condition1 = new ByCondition() { // проверяем массив на четность
            @Override
            public boolean isOk(int number) {
                if ( number %2 == 0) { // если остаток от деления равен 0
                    return true;
                } else {
                    return false;
                }
            }
        };

        ByCondition condition2 = new ByCondition() { // проверчем является ли сумма цифр элемента четным числом
            @Override
            public boolean isOk(int number) {
                int sum =0;
                while ( number != 0){
                    sum += number % 10; // берем последнюю чифру
                    number= number/ 10;
                }
                if ( sum %2 == 0) { // если остаток от деления равен 0 то четное число
                    return true;
                } else {
                    return false;
                }
            }
        };

        res = Sequence.filter(array, condition1);
        System.out.println(" Исходный  массива - " + Arrays.toString(array));
        System.out.println(" Четные числа из массива - " + Arrays.toString(res));

        res = Sequence.filter(array, condition2);
        System.out.println(" Сумма цифр элемента массива четное число - " + Arrays.toString(res));


    }
}
