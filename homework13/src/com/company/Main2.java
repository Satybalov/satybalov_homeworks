package com.company;

import java.util.Arrays;

public class Main2 {

    public static void main(String[] args) {
        // создаем массив с числами
        int[] array = { 125, 46, 248, 475, 59, 16, 3, 4 };
        int[] resultArray = new int[array.length];
        System.out.println(" Исходный  массива - " + Arrays.toString(array));

        // получаем массив четных чисел
        resultArray = Sequence.filter(array, number ->number %2 == 0 );
        System.out.println(" Четные числа из массива - " + Arrays.toString(resultArray));

        // получаем массив у которого суммоа цифр элемента массива четное число
        resultArray = Sequence.filter(array, number -> {int sum =0;
            while ( number != 0){
                sum += number % 10; // берем последнюю чифру
                number= number/ 10;
            }
            if ( sum %2 == 0) { // если остаток от деления равен 0 то четное число
                return   true;
            } else {
              return false;
            }});
        System.out.println(" Сумма цифр элемента массива четное число - " + Arrays.toString(resultArray));


    }
}
