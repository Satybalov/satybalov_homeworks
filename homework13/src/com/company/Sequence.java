package com.company;

public class Sequence {

    public static int[] filter( int[] array, ByCondition condition ){
        int arrayResult[] = new int[array.length];// Результирующий массив
        int countResult = 0; // количество элементов удовлетворяющих условию
        for (int i = 0; i < array.length; i++) {
            if (condition.isOk(array[i])) {  // если удовлетворяет условию
                  arrayResult[countResult]= array[i];
                  countResult += 1;
                //arrayResult[i]= array[i];

            }
        }
        return arrayResult;
    }
}
