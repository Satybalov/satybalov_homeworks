package com.company;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class UsersRepositoryFileImpl implements UsersRepository {

    private String fileName;


    public UsersRepositoryFileImpl(String fileName) {
        this.fileName = fileName;
    }

    @Override
    public List<User> findAll() {
        List<User> users = new ArrayList<>();
        // объявили переменные для доступа
        Reader reader = null;
        BufferedReader bufferedReader = null;
        try {
            // создали читалку на основе файла
            reader = new FileReader(fileName);
            // создали буферизированную читалку
            bufferedReader = new BufferedReader(reader);
            // прочитали строку
            String line = bufferedReader.readLine();
            // пока к нам не пришла "нулевая строка"
            while (line != null) {
                // разбиваем ее по |
                String[] parts = line.split("\\|");
                // берем имя
                String name = parts[0];
                // берем возраст
                int age = Integer.parseInt(parts[1]);
                // берем статус о работе
                boolean isWorker = Boolean.parseBoolean(parts[2]);
                // создаем нового человека
                User newUser = new User(name, age, isWorker);
                // добавляем его в список
                users.add(newUser);
                // считываем новую строку
                line = bufferedReader.readLine();
            }

        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        } finally {
            // этот блок выполнится точно
            if (reader != null) {
                try {
                    // пытаемся закрыть ресурсы
                    reader.close();
                } catch (IOException ignore) {}
            }
            if (bufferedReader != null) {
                try {
                    // пытаемся закрыть ресурсы
                    bufferedReader.close();
                } catch (IOException ignore) {}
            }
        }

        return users;
    }

    @Override
    public void save(User user) {
        Writer writer = null;
        BufferedWriter bufferedWriter = null;
        try {
            writer = new FileWriter(fileName, true);
            bufferedWriter = new BufferedWriter(writer);

            bufferedWriter.write(user.getName() + "|" + user.getAge() + "|" + user.isWorker());
            bufferedWriter.newLine();
            bufferedWriter.flush();
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        } finally {
            if (writer != null) {
                try {
                    writer.close();
                } catch (IOException ignore) {}
            }
            if (bufferedWriter != null) {
                try {
                    bufferedWriter.close();
                } catch (IOException ignore) {}
            }
        }
    }

    @Override  //выбрать людей определенного возраста
    public List<User> findByAge(int age) {
        List<User> userList = new ArrayList<>(); // массив для хранения людей
        userList = findAll();// записываем в список всех людей из файла
        // создаем новый спиок для записа результата
        List<User> userListByAge = new ArrayList<>();
        // перебираем список и отбираем определенного возраста
        for (User user : userList) {
            if (user.getAge() == age) {
                userListByAge.add(user);
            }

        }
        return userListByAge;
    }

    @Override // выбрать людей кооторые работают
    public List<User> findByIsWorkerIsTrue() {

        List<User> userList = new ArrayList<>(); // массив для хранения людей
        userList = findAll();// записываем в список всех людей из файла
        // создаем новый спиок для записа результата
        List<User> userListWorkerIsTrue = new ArrayList<>();
        // перебираем список и отбираем тех кто работает
        for (User user : userList) {
            // если работает, то записываем в новый массив
            if (user.isWorker()) {
                userListWorkerIsTrue.add(user);
            }

        }
        return userListWorkerIsTrue;

    }
}
