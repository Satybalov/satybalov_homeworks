package com.company;

import java.util.List;
import java.util.Scanner;


public class Main {
    public static void main(String[] args) {
        UsersRepository usersRepository = new UsersRepositoryFileImpl("users.txt");
        List<User> users = usersRepository.findAll();

//        for (User user : users) {
//               System.out.println(user.getAge() + " " + user.getName() + " " + user.isWorker());
//        }

        System.out.println("Введите возраст для отбора из списка");
        Scanner in = new Scanner(System.in);
        // запускаем цикл для ввода с клавиатуры возраста
        for (String ageString = in.nextLine(); ageString != ""; ageString = in.nextLine()) {
            try {
                int age = Integer.parseInt(ageString);
                List<User> users2 = usersRepository.findByAge(age);
                // если результат отбора не пуст
                if (!users2.isEmpty()) {

                    for (User user : users2) {
                        System.out.println(user.getName() + " " + user.getAge() + " " + user.isWorker());
                    }
                } else {
                    System.out.println("В списке нет никого данного возраста");
                }

            } catch (NumberFormatException e) {
                System.err.println("ввели не корректные данные " +"("+e.getMessage()+")" );


            }
            System.out.println("Введите следующий возраст, выход просто - ENTER");
        }
        System.out.println();
        System.out.println("Списор тех кто работает в настоящее время:");
        List<User> users2 = usersRepository.findByIsWorkerIsTrue();
        for (User user :users2) {
            System.out.println(user.getAge() + " " + user.getName() + " " + user.isWorker());
        }



 //       User user = new User("Игорь", 33, true);
//        usersRepository.save(user);
    }
}
