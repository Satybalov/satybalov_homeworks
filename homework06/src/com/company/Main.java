package com.company;



public class Main {
    /**функция которая принимает на вход массив и целое число
     * а выдает индекс данного числа в массиве, если число отсутствует вернуть -1
     * massiv - массив целых чисел
     */
    public static int getIndexFromMassiv(int[] massiv, int chislo  ) {
        int ires = -1;

        // ищем вхождение числа в массиве


        for (int i = 0; i < massiv.length; i++) {

            if (massiv[i] == chislo) {
                ires = i;
            }
        }
        return ires;
    }

    /**
     * Сортировка и вывод массива в консоль
     * @param massiv
     */
    public static void sortMassiv(int[] massiv){
        // сортировка
        for(int i = massiv.length-1 ; i > 0 ; i--){
            for(int j = 0 ; j < i ; j++){
            //Сравниваем элементы попарно,
            //  если они имеют неправильный порядок,
            //  то меняем местами
               if(massiv[j] < massiv[j+1] ){
                   int tmp = massiv[j];
                   massiv[j] = massiv[j+1];
                   massiv[j+1] = tmp;
               }
            }
        }
        //выводим массив в консоль
        for (int i = 0; i < massiv.length; i++) {
            System.out.println("a" + i +" = "+ massiv[i]);

        }

    }



    public static void main(String[] args) {
	// write your code here
        int[] a = {125,245,0,0,587,8,0,548,0,354,587,245};
        int b = 587;
        int result = getIndexFromMassiv(a,b);
        System.out.println("idex =" + result +"  chislo= "+ a[result]);

        sortMassiv(a);

    }
}
