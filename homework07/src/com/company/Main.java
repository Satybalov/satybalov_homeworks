package com.company;

public class Main {


    // расчет и вывод  числа с минималбным вхождением в последоваательность
    // на вход надо передать отсортированный массив
    public static int getMinKolvoChiFromMassiv(int[] massiv) {

        int kolvo = -1; // количество входжений числа
        int minChislo = -1 ;  //число с минимальным вхождением
        int minKolvo = -1; // количество вхождений
                // перебираем массив начиная с начала
        for(int i = 0; i < massiv.length; ++i) {

            kolvo = 1; // первому новому числу присваиваем кол-во вхождений равной 1
            // смотрим следующие числа начиная с текущей
            for(int j = i+1; j < massiv.length; ++j) {
                // если текущее число такое же как и следующее то прибавляем количество
                if (massiv[i] == massiv[j]) {
                     kolvo =  kolvo + 1;
                } else{ // если следующее число другое
                    // в начале цикла минимальным количеством вхождений берем равное
                    // количеству вхождений первого числа в массиве
                    if (i==0) {
                         minKolvo = kolvo;
                    };
                    // сравниваем найденное ранее минимальное вхождение с текущим
                    if (minKolvo >= kolvo)  {
                         minChislo = massiv[i];
                         minKolvo = kolvo;
                    }
                    i=j-1; //переходим к последнему выбранному числу в последовательности
                    break;
                 }
            }
        }
         ;
        return minChislo;
    }
// сортировка массива
    public static int[] sortMassiv(int[] massiv) {
        int i;
        for(i = massiv.length - 1; i > 0; --i) {
            for(int j = 0; j < i; ++j) {
                if (massiv[j] < massiv[j + 1]) {
                    int tmp = massiv[j];
                    massiv[j] = massiv[j + 1];
                    massiv[j + 1] = tmp;
                }
            }
        }

          //for(i = 0; i < massiv.length; ++i) {
           // System.out.println("a" + i + " = " + massiv[i]);
        //}

        return massiv;

    }

    public static void main(String[] args) {
        int[] posChisArray = new int[]{99, 52, 52, 52, 99, -5, 25, -5, 25, -5, 25, 99, 52, 25};
        int chislo;

        // отсортирум последовательность
        posChisArray = sortMassiv(posChisArray);


        //получаем число с минимальным вхождением в последовательность из отсоритрованного массива
        chislo = getMinKolvoChiFromMassiv(posChisArray);

        System.out.println("Число с минимальным вхождением =" + chislo );

    }
}
