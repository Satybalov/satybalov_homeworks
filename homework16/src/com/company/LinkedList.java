package com.company;


public class LinkedList<T> {

    private static class Node<T> {
        T value;
        Node<T> next;

        public Node(T value) {
            this.value = value;
        }
    }

    private Node<T> first;
    private Node<T> last;
    private int size;

    public void add(T element) {
        // создаю новый узел
        Node<T> newNode = new Node<>(element);
        if (size == 0) {
            first = newNode;
        } else {
            last.next = newNode;
        }
        last = newNode;
        size++;
    }

    public void addToBegin(T element) {
        Node<T> neNwode = new Node<>(element);

        if (size == 0) {
            last = neNwode;
        } else {
            neNwode.next = first;
        }
        first = neNwode;
        size++;
    }

    public int size() {
        return size;
    }

    public T get(int index) {

        Node<T> tnode;

        // если количество элементов больше нуля и индех не выходит за пределы размера
        if ((size > 0) && (index < size)) {
            tnode = first; // начинаем с начала и перебираем до нужжного элемента
            for (int i = 1; i <= index; i++) {
                tnode = tnode.next;
            }
            return tnode.value;
        }
        return null;
    }

}
